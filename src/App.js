import React from 'react';
// import logo from './logo.svg';
import './App.css';
import mainPage from './components/main/main.jsx';
import blue from './components/blue_card/blue.jsx';
import green from './components/green_card/green.jsx';
import pink from './components/pink_card/pink.jsx';
import purple from './components/purple_card/purple.jsx';
import red from './components/red_card/red.jsx';
import yellow from './components/yellow_card/yellow.jsx';

import {
  BrowserRouter as Router,
  HashRouter,
  Switch,
  Route,
} from "react-router-dom";


function App() {
  return (
    <div className="App">
    <Router>
      <HashRouter>
        <Switch>
            <Route path="/" exact component={mainPage} />
            <Route path="/red" exact component={red} />
            <Route path="/blue" exact component={blue} />
            <Route path="/green" exact component={green} />
            <Route path="/pink" exact component={pink} />
            <Route path="/purple" exact component={purple} />
            <Route path="/yellow" exact component={yellow} />
        </Switch>
      </HashRouter>
    </Router>
    </div>



    // <div className="App">
    //    <header className="App-header">
    //      <img src={logo} className="App-logo" alt="logo" />
    //      <h1>Hi, this is the first react app by supriya</h1>
    //      <p>
    //       Edit <code>src/App.js</code> and save to reload.
    //     </p>
    //     <a
    //       className="App-link"
    //       href="https://reactjs.org"
    //       target="_blank"
    //       rel="noopener noreferrer"
    //     >
    //       Learn React
    //     </a> 
    //   </header>
    // </div>
  );
}

export default App;
