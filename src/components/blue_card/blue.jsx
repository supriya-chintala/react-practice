import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'
import{ Button} from 'react-bootstrap'

import './blue.scss'

export default class blue extends Component {
   render() {
      return (
         
            <body>
               <h1 className="blue">Blue color</h1>
               <NavLink to=''>
                  <Button className="">Go back</Button>
               </NavLink>
            </body>
        
      )}
}  
