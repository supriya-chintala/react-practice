import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'
import { Button }  from 'react-bootstrap'

import './yellow.scss'

export default class yellow extends Component {
   render() {
      return (
         <body>
            <h1 className="yellow">yellow color</h1>
            <NavLink to=''>
               <Button className="">Go back</Button>
            </NavLink>
         </body>
      )}
}  
