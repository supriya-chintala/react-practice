import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'
import { Button } from 'react-bootstrap'
import './green.scss'

export default class green extends Component {
   render() {
      return (
         <body>
            <h1 className="green">Green color</h1>
            <NavLink to=''>
               <Button className="">Go back</Button>
            </NavLink>
         </body>
      )}
}  
