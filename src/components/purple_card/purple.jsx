import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'
import { Button } from 'react-bootstrap'

import './purple.scss'

export default class purple extends Component {
   render() {
      return (
         <body>
            <h1 className="purple">Purple color</h1>
            <NavLink to=''>
               <Button className="">Go back</Button>
            </NavLink>
         </body>
      )}
}  
