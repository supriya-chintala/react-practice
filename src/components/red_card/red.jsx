import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'
import { Button }  from 'react-bootstrap'
import './red.scss'

export default class red extends Component {
   render() {
      return (
         <body>
            <h1 className="red">Red color</h1>
            <NavLink to=''>
               <Button className="">Go back</Button>
            </NavLink>
         </body>
      )}
}  
