
import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'
import { Button } from 'react-bootstrap'

import './pink.scss'

export default class pink extends Component {
   render() {
      return (
         <body>
            <h1 className="pink">Pink color</h1>
            <NavLink to=''>
               <Button className="">Go back</Button>
            </NavLink>
         </body>
      )}
}  
