import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'
import{ Container, Row, Col, Card, Button} from 'react-bootstrap'
import './main.scss'

export default class mainPage extends Component {
   render() {
      return (
         <div className="">
            <h1>Hi, this is the first react app by supriya</h1>
            
            <Container>
               <Row className="row">
                  <Col md={6} className="color color--red">
                        <Card>
                           <Card.Title className="title" >Red color</Card.Title>
                           <Card.Body className="body">Red is the color of fire and blood, so it is associated with energy, war, danger, strength, power, determination as well as passion, desire, and love. Red is a very emotionally intense color. It enhances human metabolism, increases respiration rate, and raises blood pressure.</Card.Body>
                        </Card>
                        <NavLink to='red'>
                           <Button className="title">Know more about Red</Button>
                        </NavLink>
                  </Col>
                  <Col md={6} className="color color--blue">
                        <Card>
                           <Card.Title className="title title--blue" >Blue color</Card.Title>
                           <Card.Body className="body">Blue is the color of the sky and sea. It is often associated with depth and stability. It symbolizes trust, loyalty, wisdom, confidence, intelligence, faith, truth, and heaven. Blue is considered beneficial to the mind and body. Blue is considered beneficial to the mind and body.</Card.Body>
                        </Card>
                        <NavLink to='blue'>
                           <Button className="title title--blue">Know more about Blue</Button>
                        </NavLink>
                  </Col>
                  <Col md={6} className="color color--green">
                        <Card>
                           <Card.Title className="title title--green" >Green color</Card.Title>
                           <Card.Body className="body">Green, the color of life, renewal, nature, and energy, is associated with meanings of growth, harmony, freshness, safety, fertility, and environment. Green color is also traditionally associated with money, finances, banking, ambition, greed, jealousy, and wall street.</Card.Body>
                        </Card>
                        <NavLink to='green'>
                           <Button className="title title--green">Know more about Green</Button>
                        </NavLink>
                  </Col> 
               </Row>

               <Row>
                 <Col md={6} className="color color--pink">
                        <Card>
                           <Card.Title className="title title--pink" >Pink color</Card.Title>
                           <Card.Body className="body">The color pink is the color of universal love of oneself and of others. Pink represents friendship, affection, harmony, inner peace, and approachability. Pink is the official color for little girls and represents sugar and spice and everything nice. Pink is the sweet side of the color red.</Card.Body>
                        </Card>
                        <NavLink to='pink'>
                           <Button className="title title--pink">Know more about Pink</Button>
                        </NavLink>
                  </Col>
                  <Col md={6} className="color color--yellow">
                        <Card>
                           <Card.Title className="title title--yellow" >Yellow color</Card.Title>
                           <Card.Body className="body">On one hand yellow stands for freshness, happiness, positivity, clarity, energy, optimism, enlightenment, remembrance, intellect, honor, loyalty, and joy, but on the other, it represents cowardice and deceit. A dull or dingy yellow may represent caution, sickness, and jealousy.</Card.Body>
                        </Card>
                        <NavLink to='yellow'>
                           <Button className="title title--yellow">Know more about Yellow</Button>
                        </NavLink>
                  </Col>
                  <Col md={6} className="color color--purple">
                        <Card>
                           <Card.Title className="title title--purple" >Purple color</Card.Title>
                           <Card.Body className="body">Purple combines the calm stability of blue and the fierce energy of red. The color purple is often associated with royalty, luxury, power, and ambition. Purple also represents meanings of wealth, creativity, wisdom, dignity, grandeur, devotion, peace, pride, mystery, independence, and magic.</Card.Body>
                        </Card>
                        <NavLink to='purple'>
                           <Button className="title title--purple">Know more about Purple</Button>
                        </NavLink>
                  </Col> 
               </Row>
            </Container>


          </div>
      )}
}  
